var map;
var geocoder;

function initMap() {
    var mapOptions = {
        center: { lat: 0, lng: 0 },
        zoom: 16,
        mapTypeId: "satellite",
        heading: 0,
        tilt: 0
    };

    map = new google.maps.Map(document.getElementById("map"), mapOptions);
    geocoder = new google.maps.Geocoder();

    var headingSlider = document.getElementById("heading-slider");
    var headingValue = document.getElementById("heading-value");

    headingSlider.addEventListener("input", function() {
        var heading = parseInt(this.value, 10);
        if (!isNaN(heading)) {
            map.setHeading(heading);
            headingValue.textContent = heading;
        }
    });
}

function geocodeAddress() {
    var address = document.getElementById("address").value;
    geocoder.geocode({ address: address }, function (results, status) {
        if (status === "OK") {
            map.setCenter(results[0].geometry.location);
        } else {
            console.error("Geocoding failed: " + status);
            alert("Geocoding failed. Please check the console for more details.");
        }
    });
}

function downloadMapImage() {
    var center = map.getCenter();
    var zoom = map.getZoom();
    var heading = map.getHeading();
    var size = "1920x1080";  // Adjust the size as needed

    var staticMapUrl = "https://maps.googleapis.com/maps/api/staticmap?center=" + center.lat() + "," + center.lng() +
        "&zoom=" + zoom + "&size=" + size + "&maptype=satellite" + "&heading=" + heading + "&key=" + API_KEY;

    var link = document.createElement("a");
    link.href = staticMapUrl;
    link.target = "_blank";
    link.download = "map_image.png";
    link.click();
}

// Load the Google Maps JavaScript API
var script = document.createElement('script');
script.src = 'https://maps.googleapis.com/maps/api/js?key=' + API_KEY + '&callback=initMap';
script.async = true;
script.defer = true;
document.head.appendChild(script);
